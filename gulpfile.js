//include gulp
var gulp   = require('gulp');

//include plugins
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-ruby-sass');
var gulpFilter = require('gulp-filter');
var order = require("gulp-order");
var mainBowerFiles = require('main-bower-files');
var minifyCss = require('gulp-minify-css');
var plumber = require('gulp-plumber');
var notify = require("gulp-notify");
var plugins = require("gulp-load-plugins")({
	pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
	replaceString: /\bgulp[\-.]/
});

// Define default destination folder
var dest = 'vendor/'; 

// gulp tasks
gulp.task('scripts', function() {
  return gulp.src('js/*.js')
  .pipe(concat('global.js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('build/js'));
});

gulp.task('sass', function() {
  return sass('sass/styles.scss', {style: 'compressed'})
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('css'))
  .pipe(notify("Styles task complete"));
});

//This copies all files without minifiing
gulp.task('bower-js', function() {
  var files = mainBowerFiles('**/*.min.js');
	gulp.src(plugins.mainBowerFiles().concat(files))
		.pipe(plugins.filter('*.js'))
		//.pipe(plugins.concat('bower.js'))
		//.pipe(plugins.uglify())
		.pipe(gulp.dest(dest));
});


/*gulp.task('bower-css', function() {
	var paths = ['styles']
	gulp.src(plugins.mainBowerFiles().concat(paths))
		.pipe(plugins.filter('*.scss'))
		.pipe(plugins.order([
			'normalize.scss',
			'*'
		]))
		.pipe(plugins.concat('bower.css'))
		.pipe(plugins.minifyCss())
		.pipe(gulp.dest(dest));
});*/

//gulp watch
gulp.task('watch', function() {
  gulp.watch('js/*.js', ['scripts']);
  gulp.watch('sass/*.scss', ['sass']);
});

// default task
gulp.task('default', ['scripts', 'sass', 'bower-js', 'watch']);