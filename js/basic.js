(function ($) {
  
  // STICKY HEADER
  var options = {
    offset: 200
  }
  var banner = new Headhesive('#block-svdhe-main-menu', options);
  
  // Toggle Mobile Menue
  $( "#block-mainnavigation-2 h2" ).click(function() {
    $( "#block-mainnavigation-2 ul" ).toggleClass( "show" );
    $(this).text($(this).text() == 'Menu' ? 'Schliessen' : 'Menu');
    return false;
  });
  
  $("form :input").each(function(index, elem) {
    var eId = $(elem).attr("id");
    var label = null;
    if (eId && (label = $(elem).parents("form").find("label[for="+eId+"]")).length == 1) {
        $(elem).attr("placeholder", $(label).html());
        $(label).remove();
    }
 });

})(jQuery);